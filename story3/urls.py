from django.conf import settings
from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='story3-home'),
    path('profile/', views.profile, name='story3-profile'),
    path('project/', views.project, name='story3-project'),
    path('about/', views.about, name='story3-about'),
    path('contact/', views.contact, name='story3-contact'),
]